const cacheFunction = require('../cacheFunction.js');

function subtract(val1, val2) {

    return val1 - val2;
}

const invokeCb = cacheFunction(subtract);

console.log(invokeCb(2,1));
console.log(invokeCb(2,2));
console.log(invokeCb(3,4));
console.log(invokeCb(2,1));



