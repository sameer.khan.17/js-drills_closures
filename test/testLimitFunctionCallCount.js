const limitFunctionCallCount = require('../limitFunctionCallCount.js');

function sayHello() {
    console.log("hello world");
}

const invokeCb = limitFunctionCallCount(sayHello, 5);

for (let i = 0; i < 10; i++)
{
    invokeCb();
}
