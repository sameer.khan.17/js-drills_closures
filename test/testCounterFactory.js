const counterFactory = require('../counterFactory.js');

const counter1 = counterFactory();

console.log(counter1.increment(2));

console.log(counter1.decrement(3));
