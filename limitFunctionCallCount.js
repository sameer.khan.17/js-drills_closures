function limitFunctionCallCount(cb, n) {
    if (cb == undefined || n == undefined) {
        return null;
    }
    else {
        let cbCalls = 0;
        function invokeCb() {
            if (cbCalls < n) {
                cbCalls++;
                cb();
            }
            else {
                return null;
            }

        }
        return invokeCb;
    }
}
module.exports = limitFunctionCallCount;

