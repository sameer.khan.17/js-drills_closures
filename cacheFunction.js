function cacheFunction(cb) {
    if (cb == undefined) {
        return null;
    }
    else {
        const cache = {};

        function invokeCb() {
            let args = [];

            for (let i = 0; i < arguments.length; i++) {
                args.push(arguments[i]);
            }

            if (cache[args] == undefined) {
                cache[args] = cb(...args);
                return cache[args];
                
            }
            else {
                return cache[args];

            }

        }
        return invokeCb;
    }
}

module.exports = cacheFunction;


