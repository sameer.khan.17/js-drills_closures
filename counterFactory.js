function counterFactory() {
    let counter = 0;
    function increment(val) {
        counter += val;
        return counter;
    }
    function decrement(val) {
        counter -= val;
        return counter;
    }
    return {
        increment: increment,
        decrement: decrement
    };

}
module.exports = counterFactory;